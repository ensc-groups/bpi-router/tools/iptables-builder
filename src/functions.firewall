#! /bin/bash

: ${IPTABLES:=iptables}
: ${IS_IPV4:=false}
: ${IS_IPV6:=false}
: ${IS_DRYRUN:=false}
: ${ONLY_XML:=false}
$IS_IPV4 && : ${IPTABLES_RESTORE:=iptables-restore}
$IS_IPV6 && : ${IPTABLES_RESTORE:=ip6tables-restore}
$IS_IPV4 && : ${BAD_FILE:=/var/run/firewall-ip4.bad}
$IS_IPV6 && : ${BAD_FILE:=/var/run/firewall-ip6.bad}
LOCK_FILE=/var/lock/firewall@all.lock
IPTABLES_FLAGS=''

M_ROOT='-m owner --uid-owner root'
M_OWNER='-m owner --uid-owner'
M_COMMENT='-m comment --comment'
NOSYN='! --tcp-flags SYN SYN'

current_table=''
current_chain=''

function _cleanup {
    ! test -d "$W" || rm -rf "$W"
    W=
    trap "" EXIT
}

function _klog {
    ! $IS_DRYRUN || return 0

    local l=$1
    shift
    echo "<$l>$*" > /dev/kmsg 2>/dev/null || :
}

function log {
    _klog 7 "$@"
}

function panic {
    _klog 4 "$@"
    echo "*** $*" >&2
    exit 1
}

function warning {
    _klog 4 "$@"
    echo "*** $*" >&2
}

function quote {
    local delim=$1
    local i
    shift

    for i; do
	echo -n "$delim"
	delim=' '

	if test x"${i//[-_a-zA-Z0-9+:\/=.,\!\[\]]/}" = x; then
	    echo -n $i
	    continue
	fi

	i=${i//\\/\\\\}
	i=${i//"/\\"}
	printf "\"%s\"" "$i"
    done
}

function emit {
    quote '' "$@"
    echo
}

function start {
    W=`mktemp -d -t firwall.XXXXXX` || panic "failed to create workdir"

    {
	local p
	p=`type -p "$IPTABLES_RESTORE"` || p=$IPTABLES_RESTORE
	echo "#! $p --wait=10"
	echo "## Generation started at `LANG=C date`"
    } > $W/header
    selectTable filter

    ! test -e "$1" || rm -f "$1"

    trap "commit \"$1\"" EXIT
}

function commit_table {
    local d=$W/tables/$1

    test -d "$d" || return 0
    ! test -e "$d/.commited" || return 0

    touch "$d/.commited"

    local f

    quote "*" "$1"
    echo

    f="$d/policies"
    if test -e "$f"; then
	sort -u "$f" | sed 's/^/:/'
    fi

    f="$d/chains"
    if test -e "$f"; then
	sort -u "$f" | sed 's/^/:/'
    fi

    f="$d/rules"
    if test -e "$f"; then
	cat "$f"
    fi

    echo COMMIT
}

function _commit {
    local t

    cat $W/header

    for i in raw mangle nat filter; do
	commit_table "$i"
    done

    for t in $W/tables/*; do
	commit_table "${t##$W/tables/}"
    done

    echo "## finished at `LANG=C date`"
}

function _exec_fallback {
    function execw {
	$IPTABLES "$@" || \
	    warning "fallback: failed to execute $*"
    }

    function selectTable {
	case x$1 in
	  x|xfilter)
		;;
	  *)
		warning "fallback: selecting non filter table not supported"
		;;
	esac

	: # noop
    }

    warning "installing fallback rules"

    for t in $W/tables/*; do
	n=${t##$W/tables/}
	execw -t $n -F
	execw -t $n -X
    done

    _cleanup
    enableFallback

    exit 1
}

function _expand {
    local _expand_var=$1
    local _expand_fn=$2
    local _expand_tmp
    local _expand_opt

    shift 2

    if test -z "$_expand_var"; then
	local _expand_args=( )
    else
	eval _expand_tmp=(\ \"\${$_expand_var[@]}\"\ )
	local _expand_args

	_expand_args=( "${_expand_tmp[@]}" "$1" )
	shift
    fi

    : DBG "$@"

    while test $# -ge 1; do
	_expand_opt=$1
	shift

	case $_expand_opt in
	  @@*)
		_expand_args+=( "@${_expand_opt##@@}" )
		;;

	  @*@|@@*)
		## the '@@*' is just a hack to fix emacs indentation;
		## case is handle above already

		_expand_var=$_expand_opt
		_expand_var=${_expand_var##@}
		_expand_var=${_expand_var%%@}

		declare -p "$_expand_var" &>/dev/null || {
		    echo "variable '$_expand_var' does not exist" >&2
		    return 1
		}
		#eval _expand_tmp=\${$_expand_var?} || return

		eval _expand_tmp='( "${'$_expand_var'[@]}" )'
		for _expand_opt in "${_expand_tmp[@]}"; do
		    _expand _expand_args "$_expand_fn" "$_expand_opt" "$@" || return $?
		    do_skip=true
		done

		return 0
		;;

	  *)
		_expand_args+=( "$_expand_opt" )
		;;
	esac
    done

    "$_expand_fn" "${_expand_args[@]}"
}

function commit {
    local args=

    _commit > $W/firewall.xml

    if ! $ONLY_XML; then
	! $IS_DRYRUN || args=--test

	$IS_DRYRUN || log "starting to commit iptables rules"
	{
	    flock 99

	    $IPTABLES_RESTORE $args "$W/firewall.xml" 99<&- || {
		warning "failed to commit rules"
		rm -f $BAD_FILE
		cp $W/firewall.xml $BAD_FILE
		$IS_DRYRUN || _exec_fallback
	    } 99<&-
	} 99> $LOCK_FILE
	$IS_DRYRUN || log "commited iptables rules"
    fi

    test "$#" -ge 1 -a "$1" != '' || return
    cp $W/firewall.xml "$1"
    chmod +x "$1"
    _cleanup
}

function flushAll {
    : echo "WARNING: obsolete 'flushAll' called" >&2
}

function selectChain {
    current_chain=$1
}

function addChain {
    selectChain "$1"
    emit "$1" "-" "[0:0]" >> "$W/t/chains"
}

# Usage: addChainInput <name-suffix> <ip> [<lo-ip>]
function addChainInput {
    local suffix=$1
    local ip=$2
    local lo_ip=$3/24

    unset self
    unset lo4

    addChain "i-$suffix"
    dispatch_register INPUT -d "$ip"
    $IS_IPV4 && test -n "$lo_ip" && dispatch_register i_LO -d "$lo_ip"

    # export global 'lo4' variable
    test -z "$lo_ip" || lo4=$lo_ip

    # export 'self_ip' variable
    self=$ip
}

# Usage: addChainInput <name-suffix> <ip> [<lo-ip>]
function addChainOutput {
    local suffix=$1
    local ip=$2
    local lo_ip=$3/24

    unset self
    unset lo4

    addChain "o-$suffix"
    dispatch_register OUTPUT -s "$ip"
    $IS_IPV4 && test -n "$lo_ip" && dispatch_register o_LO -s "$lo_ip"

    # export global 'lo4' variable
    test -z "$lo_ip" || lo4=$lo_ip

    # export 'self_ip' variable
    self=$ip
}

function selectTable {
    current_table=$1

    mkdir -p "$W/tables/$1"
    rm -f "$W/t"
    ln -s "tables/$1" "$W/t"
}

function setPolicy {
    emit "$current_chain" "$1" "[0:0]" >> "$W/t/policies"
}

function _add {
    test -n "$current_chain" || panic 'No chain selected; aborting'
    emit -A "$current_chain" "$@" >> "$W/t/rules"
}

function add {
    _expand '' _add "$@"
}

function add_ipv4 {
    ! $IS_IPV4 || add "$@"
}

function add_ipv6 {
    ! $IS_IPV6 || add "$@"
}

function insert {
    test -n "$current_chain" || panic 'No chain selected; aborting'
    emit -I "$current_chain" "$@" >> "$W/t/rules"
}

function insert_ipv4 {
    ! $IS_IPV4 || insert "$@"
}

function insert_ipv6 {
    ! $IS_IPV6 || insert "$@"
}

function genrules {
    local prefix=$1
    local suffix=$2
    shift 2

    while test $# -ge 2; do
	local r=$1
	local s
	for s in $2; do
	    eval ${prefix}_${s}_${suffix}+='( "$r" )'
	done
	shift 2
    done

    test $# -eq 0 || panic "Incomplete rule-pairs"
}

function dispatch_clear() {
    rm -f "$W/t/"dispatch-*
}

function _dispatch_register {
    local chain=$1
    shift
    emit -A @@CHAIN@@ "$@" -g "$current_chain" >> "$W/t/dispatch-$chain"
}

function dispatch_register {
    _expand '' _dispatch_register "$@"
}

function dispatch_emit {
    ! test -e "$W/t/dispatch-$1" || \
	sed "s!@@CHAIN@@!$current_chain!g" < "$W/t/dispatch-$1" >> "$W/t/rules"
}

function add-many {
    common=$1
    shift

    while test $# -ge 1; do
	add $common $1
	shift
    done
}

function add-dst {
    local target

    case $1 in
      -[jg])
	    target="$1 $2"
	    shift 2
	    ;;

      -c)
	    local tmp=()
	    local fn

	    fn=$2
	    shift 2
	    $fn tmp "$@"
	    add-dst-vec "${tmp[@]}"
	    return
	    ;;

      *)
	    target="${DEFAULT_TARGET:--j ACCEPT}"
	    ;;
    esac

    local dest=$1
    local proto=$2
    local port=$3
    shift 3

    case $dest in
      ALL)	dest=0.0.0.0/0;;
      *)	;;
    esac

    case $proto in
      ALL)	proto=;;
      *)	proto="-p $proto";;
    esac

    case $port in
      0)	port=;;
      *)	port="--dport $port";;
    esac

    add $proto -d "$dest" $port "$@" $target
}

function add-dst-vec {
    for i; do
	eval add-dst $i
    done
}

function add-mac-spoof {
    local src=$1
    local mac

    shift
    case "$1" in
	(!)	mac=( -m mac ! --mac-source "$2" );       shift 2;;
	(!*)	mac=( -m mac ! --mac-source "${1##\!}" ); shift;;
        (-)     shift;;
	(*)	mac=( -m mac   --mac-source "$1" );       shift;;
    esac


    test $# -gt 0 || set -- -j RETURN
    add -s "$src" "${mac[@]}" "$@"
}

function add-mac-spoof-vec {
    local i
    for i; do
	add-mac-spoof $i
    done
}

function add-mac-block {
    local mac
    shift

    case "$1" in
	(!)	mac=( ! --mac-source "$2" );       shift 2;;
	(!*)	mac=( ! --mac-source "${1##\!}" ); shift;;
	(-)	return;;
	(*)	mac=(   --mac-source "$1" );       shift;;
    esac

    test $# -gt 0 || set -- -g .ldrop
    add -m mac "${mac[@]}" "$@"
}

function add-mac-block-vec {
    local i
    for i; do
	add-mac-block $i
    done
}

function add_drop {
    add -j LOG -m limit --limit 1/s --log-prefix "${LOG_PREFIX}$1" --log-tcp-sequence --log-tcp-options --log-ip-options $log_uid --log-level 6
    add -j DROP
}

function addDefaultChains {
    local have_drop=false
    local have_reject=false
    local extra=${LOG_PREFIX:+${LOG_PREFIX}}$1

    # do not make log_uid local; it might be used outside
    case $(uname -r) in
      2.6.9-*)
	    log_uid=;;
      *)
	    log_uid=--log-uid;;
    esac

    case x$current_table in
      xnat)		have_drop=false;;
      xfilter|x)	have_drop=true; have_reject=true;;
      *)		have_drop=true;;
    esac

    if $have_drop; then
	addChain .drop
	add -j DROP

	addChain .ldrop
	add -j LOG -m limit --limit 1/s     --log-prefix "[${extra}drop]   " --log-tcp-sequence --log-tcp-options --log-ip-options $log_uid --log-level 6
	add -j DROP
    fi

    if $have_reject; then
	addChain .hide
	add -j REJECT -p tcp  --reject-with tcp-reset
	add_ipv4 -j REJECT -p icmp   --reject-with icmp-host-unreachable
	add_ipv6 -j REJECT -p icmpv6 --reject-with icmp6-addr-unreachable
	add -j REJECT

	addChain .lhide
	add -j LOG -m limit --limit 1/s --log-prefix "[${extra}hide]   " --log-tcp-sequence --log-tcp-options --log-ip-options $log_uid --log-level 6
	add -g .hide

	addChain .lreject
	add -j LOG -m limit --limit 1/s --log-prefix "[${extra}reject] " --log-tcp-sequence --log-tcp-options --log-ip-options $log_uid --log-level 6
	add -j REJECT
    fi

    addChain .watch
    add -j LOG -m limit --limit 1/s     --log-prefix "[${extra}watch]  " --log-tcp-sequence --log-tcp-options --log-ip-options $log_uid --log-level 6
    add -j ACCEPT
}

start "$1"
