srcdir ?= $(dir $(firstword ${MAKEFILE_LIST}))
VPATH = ${srcdir}

prefix ?= /usr/local
bindir ?= ${prefix}/bin
libexecdir ?= ${prefix}/libexec
pkgexecdir ?= ${libexecdir}/iptables-builder

INSTALL = install
INSTALL_BIN = ${INSTALL} -p -m 0755
INSTALL_DATA = ${INSTALL} -p -m 0644

MKDIR_P = ${INSTALL} -d -m 0755

SED = sed
SED_CMD = \
  -e 's!@pkgexecdir@!${pkgexecdir}!g'

PKGEXECDIR_DATA = \
  src/firewall-all.simple \
  src/firewall-ipv4.simple \
  src/firewall-ipv6.simple \
  src/functions.firewall \

PKGEXECDIR_PROGRAMS = \
  src/firewall-ipv4.rules \
  src/firewall-ipv6.rules \

BIN_PROGRAMS = \
  generate-iptables \

all:	${PKGEXECDIR_DATA} ${PKGEXECDIR_BIN} ${BIN_PROGRAMS}

install:	.install-libexecbin .install-libexecdata .install-bin

clean:
	rm -f ${BIN_PROGRAMS}

.install-libexecdata:		${PKGEXECDIR_DATA}
	${MKDIR_P} ${DESTDIR}${pkgexecdir}
	${INSTALL_DATA} $^ ${DESTDIR}${pkgexecdir}/

.install-libexecbin:		${PKGEXECDIR_PROGRAMS}
	${MKDIR_P} ${DESTDIR}${pkgexecdir}
	${INSTALL_BIN} $^ ${DESTDIR}${pkgexecdir}/

.install-bin:			${BIN_PROGRAMS}
	${MKDIR_P} ${DESTDIR}${bindir}
	${INSTALL_BIN} $^ ${DESTDIR}${bindir}/

%:	%.in
	@rm -f $@
	${SED} ${SED_CMD} $< > $@
	chmod --reference '$<' '$@'
	chmod a-w '$@'
